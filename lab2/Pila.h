#include <iostream>
using namespace std;


#ifndef PILA_H
#define PILA_H

class Pila {
    private:
		int max;
        int tope = 0;
        bool band;
        int *pila = NULL;
        int dato;

    public:
        /* constructores */
        Pila(int max);
        /* métodos get and set */
        void push();
        void pop();
        void print();
        void pila_llena(int tope, int max);
        void pila_vacia(int tope);
        int get_tope();
        bool get_band();
        
};
#endif

