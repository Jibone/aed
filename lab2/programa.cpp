#include <iostream>
#include "Pila.h"
using namespace std;
#include "Pila.h"

void menu(){
	cout << "--------Menu---------" << endl;
	cout << "Agregar/Push dato [1]" << endl;
	cout << "Remover/Pop dato  [2]" << endl;
	cout << "Ver pila          [3]" << endl;
	cout << "Salir             [4]" << endl;
	cout << "---------------------" << endl;
}
int main(){
	int max;
	bool salir = false;
	int opcion;
	cout << "¿Cantidad maxima de datos en la pila?: "; cin>>max;
	Pila pila = Pila(max);
	while(salir != true){
		cout << " " << endl;
		menu();
		cout << "Opcion: "; cin>>opcion;
		cout << " " << endl;
		switch(opcion){
			case 1:
				pila.push();
				break;
			case 2:
				pila.pop();
				break;
			case 3:
				pila.print();
				break;
			case 4:
				salir = true;
				break;
		}
		
	}
	cout << "¡Hasta pronto!" << endl;
	cout << " " << endl;
}
	
