El presente programa se compone de un archivo (programa.cpp) desde el cual se hará uso de la clase Pila utilizando el .cpp y .h que esta tiene.
La finalidad de este programa es generar una pila, la cual funciona bajo un mecanismo LIFO, con un tamaño y datos ingresados por el usuario. Para ejecutar adecuadamente el programa debe se debe situar dentro la carpeta contenedora de este, utilizar el comando "make" para compilarlo y despues llamar al programa utilizando "./programa".
Una vez dentro se podrá ver que el programa es bastante simple de entender para el usuario, por lo que no presentaría dificultades su uso.
Ha sido desarrollado bajo el lenguaje C++.

