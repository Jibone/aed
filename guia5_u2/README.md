El programa consiste en la creacion de un arbol binario balanceado, el cual se va a ir creando a través de un menú interactivo, de manera que el mismo usuario podrá ir escogiendo que valores agregar, eliminar o modificar del arbol. A través del mismo menú podrá imprimir los valores del arbol en preorden, inorden y posorden, y tambien crear un grafico del arbol utilizando Graphviz (para esto debe tenerlo descargado con el comando de terminal "sudo apt-get install graphviz")

Para inicializar el programa debe localizarse a traves de la terminal dentro de la carpeta del mismo, una vez allí compilar el programa con el comando "make" y ya se podría ejecutar con "./programa".

Hecho por José Bernal.






