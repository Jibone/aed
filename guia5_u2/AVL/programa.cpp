#include <iostream>
#include "Grafo.h"
#include "Arbol.h"
using namespace std;

void menu(Arbol *arbol, Nodo *raiz){
    int opcion; 
    bool crece = false;
    int numero;
 
    while(opcion != 6){ 

		cout << "--------Menu---------" << endl;
		cout << "Ingresar dato     [1]" << endl;
		cout << "Eliminar dato     [2]" << endl;
		cout << "Modificar dato    [3]" << endl;
		cout << "Imprimir datos    [4]" << endl;
		cout << "Generar Grafo     [5]" << endl;
		cout << "Salir             [6]" << endl;
		cout << "---------------------" << endl;
        cout << "\n Ingrese su opción: ";
        cin >> opcion;
       
        switch(opcion){
            case 1:
                // Se ingresa el nuevo dato y entra como nodo
                cout << "opsion1" << endl;
                cout << "Ingrese el nuevo dato: ";
				cin >> numero;
                arbol->ingresar_nodo(raiz, numero, crece);
                break;

            case 2:
                // Se elimina el nodo que contenga el dato ingresado
                cout << "Ingrese dato a eliminar: ";
				cin >> numero;
                arbol->eliminar_nodo(raiz, crece, numero);
                break;

            case 3: 
                // Se elimina el nodo escogido y se reemplaza con un nuevo dato/nodo
				cout << "Ingrese dato a modificar: ";
				cin >> numero;
				arbol->eliminar_nodo(raiz, crece, numero);
                cout << "Ingrese el nuevo dato:";
                arbol->ingresar_nodo(raiz, numero, crece);
                break;

			case 4:
				//Se imprimen los datos en preorden, inorden y posorden
				cout<< "PREORDEN"<<endl;
				arbol->print_preorden(raiz);
				cout<< "" <<endl;
				cout<< "INORDEN"<<endl;
				arbol->print_inorden(raiz);
				cout<< "" <<endl;
				cout<< "POSORDEN"<<endl;
				arbol->print_posorden(raiz);
				break;
				
            case 5:
                // Se generará el grafico del arbol balanceado
                Grafo *g = new Grafo(raiz);
                g->crearGrafo();
                break;
        }
        system("clear");
    }
}

//Main!
int main(){
    Arbol *arbol = new Arbol();
    Nodo *raiz = NULL;
    
    cout << " -------GENERADOR DE GRAFICOS------" << endl;
    cout << " -----------------AVL--------------" << endl;
    menu(arbol, raiz);

    return 0;
}

