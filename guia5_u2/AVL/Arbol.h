#include <iostream>
using namespace std;

#ifndef ARBOL_H
#define ARBOL_H


//Se define la clase arbol y sus funciones
class Arbol {
	public:
		Arbol();
		Nodo* crear_nodo(int);
		void ingresar_nodo(Nodo *&, int, bool &);
		void reestructuracion_izq(Nodo *&, bool&);
		void reestructuracion_der(Nodo *&, bool&);
		void rotacion_DD(Nodo *&, bool&, Nodo*);
		void rotacion_DI(Nodo *&, Nodo *, Nodo *);
		void rotacion_II(Nodo *&, bool&, Nodo*);
		void rotacion_ID(Nodo *&, Nodo *, Nodo *);
		void eliminar_nodo(Nodo *&, bool&, int);
		void print_preorden(Nodo *);
		void print_inorden(Nodo *);
		void print_posorden(Nodo *);
};
#endif
