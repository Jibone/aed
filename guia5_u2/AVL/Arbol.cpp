#include <iostream>
#include "Grafo.h"
#include "Arbol.h"
using namespace std;

Arbol::Arbol() {}

//metodo para crear un nodo
Nodo* Arbol::crear_nodo(int n){
	Nodo *nodo = new Nodo();
	nodo->num = n;
	nodo->fe = 0;
	nodo->izq = NULL;
	nodo->der = NULL;
	return nodo;
}

//metodo para ingresar un nodo en el arbol, utiliza crear_nodo(), pide como parametro la raiz del arbol, el valor a ingresar y la variable "crece" indica si algun lado del arbol a sufrido cambios en su altura
void Arbol::ingresar_nodo(Nodo *&nodo, int n, bool &crece){
	if(nodo == NULL){
		nodo = crear_nodo(n);
	}
	if(n < nodo->num){
		ingresar_nodo(nodo->izq, n, crece);
		reestructuracion_der(nodo, crece);
	}
	else if (n > nodo->num){
		ingresar_nodo(nodo->der, n, crece);
		reestructuracion_izq(nodo, crece);
	}
	else{
		cout << "******El dato ingresado ya se encuentra en el arbol.*******" << endl;
		//return nodo;
	}
	crece = true;
}
//metodo para reestructurar el arbol cuando ha sido cargado con datos el lado derecho
void Arbol::reestructuracion_izq(Nodo *&nodo, bool &crece) {
    Nodo *nodo1;
    Nodo *nodo2;

    if(crece == true){
       
        if(nodo->fe== -1){
            nodo->fe = 0;
        }

        else if(nodo->fe == 0){
            nodo->fe = 1;
            crece = false;
        }

        else if(nodo->fe == 1){
            nodo1 = nodo->der;
            
            if(nodo1->fe >= 0){
               rotacion_DD(nodo, crece, nodo1);
            }

            else{
                rotacion_DI(nodo, nodo1, nodo2);
            }
        }
    }
}
//metodo para reestructurar el arbol cuando ha sido cargado con datos el lado izquierdo
void Arbol::reestructuracion_der(Nodo *&nodo, bool &crece){
    Nodo *nodo1;
    Nodo *nodo2;
    

    if(crece == true){
		
        if(nodo->fe == 1){
            nodo->fe = 0;
        }

        else if(nodo->fe == 0){
            nodo->fe = -1;
            crece = false;
        }

        else if(nodo->fe == -1){
            nodo1 = nodo->izq;
            
            if(nodo1->fe <= 0){
                rotacion_II(nodo, crece, nodo1);
            }

            else{
                rotacion_ID(nodo, nodo1, nodo2);
            }
        }
    }
}

void Arbol::rotacion_DD(Nodo *&nodo, bool &crece, Nodo *nodo1){
    nodo->der = nodo1->izq;
    nodo1->izq = nodo; 
    
    // Actualiza el factor de cambio de los nodos modificados 
    if(nodo1->fe == 0){
        nodo->fe = 1;
        nodo1->fe = -1;
        crece = false;
    }
    
    else if(nodo1->fe == 1){
        nodo->fe = 0;
        nodo1->fe = 0;
    }
    
    //se le da un nuevo valor a la raiz
    nodo = nodo1;
}

void Arbol::rotacion_DI(Nodo *&nodo, Nodo *nodo1, Nodo *nodo2){
    nodo2 = nodo1->izq;
    nodo->der = nodo2->izq;
    nodo2->izq = nodo; 
    nodo1->izq = nodo2->der;
    nodo2->der = nodo1;

    if(nodo2->fe == 1){
        nodo->fe = -1;
    }

    else{
        nodo->fe = 0;
    }

    if(nodo2->fe == -1){
        nodo1->fe = 1;
    }

    else{
        nodo1->fe = 0;
    }
    
    nodo = nodo2;
    nodo2->fe = 0;
}

void Arbol::rotacion_II(Nodo *&nodo, bool &crece, Nodo *nodo1){
    nodo->izq = nodo1->der;
    nodo1->der = nodo; 
    
    if(nodo1->fe == 0){
        nodo->fe = -1;
        nodo1->fe = 1;
        crece = false;
    }
    
    else if(nodo1->fe == -1){
        nodo->fe = 0;
        nodo1->fe = 0;
    }

    nodo = nodo1;
}

void Arbol::rotacion_ID(Nodo *&nodo, Nodo *nodo1, Nodo *nodo2){
    nodo2 = nodo1->der;
    nodo->izq = nodo2->der;
    nodo2->der = nodo; 
    nodo1->der = nodo2->izq;
    nodo2->izq = nodo1;

    if(nodo2->fe == -1){
        nodo->fe = 1;
    }

    else{
        nodo->fe = 0;
    }

    if(nodo2->fe == 1){
        nodo1->fe = -1;
    }

    else{
        nodo1->fe = 0;
    }
    
    nodo = nodo2;
    nodo2->fe = 0;
}

//metodo para eliminar un nodo del arbol, pide como parametro la raiz del arbol, la variable "crece" para registrar el cambio de altura, y "n" que es el dato del nodo a eliminar"
void Arbol::eliminar_nodo(Nodo *&nodo, bool &crece, int n){
    Nodo *tmp;
    Nodo *aux1;
    Nodo *aux2;
    bool ayuda;

    if(nodo != NULL){
        // se identifica el subárbol al que pertenece el dato
        if(n < nodo->num){
            // llamada recursiva para eliminar el número
            eliminar_nodo(nodo->izq, crece, n);
            // reestructuración de los elementos restantes
            reestructuracion_izq(nodo, crece);
        }
        
        else if(n > nodo->num){
            eliminar_nodo(nodo->der, crece, n);
            reestructuracion_der(nodo, crece);
        }
        
        else{
            tmp = nodo;
            
            if(tmp->der == NULL){
                nodo = tmp->izq;
            }
            
            else{
               
                if(tmp->izq == NULL){
                    nodo = tmp->der;
                }
                
                else{ 
                    aux1 = nodo->izq;
                    ayuda = false;
                   
                    while(aux1->der != NULL){
                        aux2 = aux1;
                        aux1 = aux1->der;
                        ayuda = true;
                    }

                    nodo->num = aux1->num;
                    tmp = aux1;
                    
                    if(ayuda == true){
                        aux2->der = aux1->izq;
                    }
                    
                    else{
                        nodo->izq = aux1->izq;
                        free(tmp); 
                    }
                }
            }
        }
    }
    
    else{
        cout << "Ese dato no existe en el arbol.";
    }
}
//Estos son los metodos para imprimir los datos del arbol, ya sea en preorden, inorden o posorden.
void Arbol::print_preorden(Nodo *arbol){
	if(arbol == NULL){
		return;
	}else{
		cout<< arbol->num << "-";
		print_preorden(arbol->izq);
		print_preorden(arbol->der);
	}
}
void Arbol::print_inorden(Nodo *arbol){
	if(arbol == NULL){
		return;
	}else{
		print_inorden(arbol->izq);
		cout<< arbol->num << "-";
		print_inorden(arbol->der);
	}
}
void Arbol::print_posorden(Nodo *arbol){
	if(arbol == NULL){
		return;
	}else{
		print_posorden(arbol->izq);
		print_posorden(arbol->der);
		cout<< arbol->num << "-";
	}
}

