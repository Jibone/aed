#include <iostream>
#include <fstream>

using namespace std;

//en este .h definimos la estructura de Nodo y la clase Grafo
typedef struct _Nodo{
    int num,fe;
    struct _Nodo *izq;
    struct _Nodo *der;
} Nodo;

class Grafo{
    private:
        Nodo *arbol = NULL;

    public:
        /* constructor*/
        Grafo(Nodo *arbol);
        
        /* Métodos de la clase Grafo */
        void crearGrafo();
        void recorrerArbol (Nodo *, ofstream &);
        
};
