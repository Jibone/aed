#include <iostream>
using namespace std;


/* clases */
#include "Arbol.h"
#include "Makearbol.h"
#include "Nodo.h"
void menu(){

	cout << "--------Menu---------" << endl;
	cout << "Ingresar dato     [1]" << endl;
	cout << "Eliminar dato     [2]" << endl;
	cout << "Modificar dato    [3]" << endl;
	cout << "Imprimir datos    [4]" << endl;
	cout << "Generar Grafo     [5]" << endl;
	cout << "---------------------" << endl;
}

int main (void) {
	bool ciclo = true;
	int ingreso;
	int opcion;
	
	Makearbol macetero = Makearbol();
	Arbol *arbol = macetero.get_arbol();
	Nodo* raiz = arbol->get_raiz();
	while(ciclo == true){
		cout << " " << endl;
		menu();
		cout << "Opcion: "; cin>>opcion;
		cout << " " << endl;
		switch(opcion){
			case 1:
				cout << "Ingrese el nuevo dato: ";
				cin >> ingreso;
				if(raiz == NULL){
					arbol->crear_nodo(ingreso);
				}
				else {
					arbol->insertar_nodo(raiz, ingreso);
				}
				cout << "--Listo--" << endl;
				break;
				
			case 2:
				cout << "Ingrese dato a eliminar: ";
				cin >> ingreso;
				arbol->eliminar_nodo(raiz, ingreso);
				cout << "--Listo-- " << endl;
				break;
			
			case 3:	
				cout << "Ingrese el dato que desea modificar: ";
				cin >> ingreso;
				arbol->eliminar_nodo(raiz, ingreso);
				cout << "Ingrese el nuevo dato: ";
				cin >> ingreso;
				arbol->insertar_nodo(raiz, ingreso);
				break;
			case 4:
				cout << "Preorden: " << endl;
				arbol->print_preorden(raiz);
				cout << " " << endl;
				cout << "Inorden: " << endl;
				arbol->print_inorden(raiz);
				cout << " " << endl;
				cout << "Posorden: " << endl;
				arbol->print_posorden(raiz);
				cout << " " <<endl;
		}
		
	}
    return 0;
}
	
	
	
	

	
