#include <iostream>
using namespace std;

//#include "Nodo.h"
#include "Arbol.h"

Arbol::Arbol() {}

//metodo que crea la lista
Nodo* Arbol::crear_nodo (int num) {
    Nodo *tmp;
    tmp = new Nodo();
    tmp->num = num;
    tmp->izq = NULL;
    tmp->der = NULL;
    if(this->raiz == NULL){
		this->raiz = tmp;
	}
    return tmp;
}

//Metodo para insertar un nodo en el arbol (ingresa ordenado)
void Arbol::insertar_nodo(Nodo *&viejo, int num) {
	if(viejo = NULL){
		viejo = crear_nodo(num);
	}
	else if(num < viejo->num){
		insertar_nodo(viejo->izq, num);
	}
	else if(num > viejo->num){
		insertar_nodo(viejo->der, num);
	}
	else{
		cout << "***El dato ingresado ya existe***" << endl;
	}
}

Nodo* Arbol::busqueda(Nodo *buscado, int num){
	
	if(buscado == NULL){
		cout<< "***El nodo buscado no existe***" <<endl;
	}
	else if(num < buscado->num){	
		busqueda(buscado->izq, num);
	}
	else if(num > buscado->num){
		busqueda(buscado->der, num);
	}
	else {
		return buscado;
	}
}

void Arbol::eliminar_nodo(Nodo *&eliminar, int n){
	Nodo* aux_der;
	Nodo* aux_izq;
	Nodo* aux_maxder;
	Nodo* aux_antesmax;
	Nodo* aux_eliminar;
	aux_eliminar = busqueda(eliminar, n);
	if(aux_eliminar->der == NULL){
		aux_eliminar = aux_eliminar->izq;
	}
	else if(eliminar->izq == NULL){
		aux_eliminar = aux_eliminar->der;
	}
	else {
		aux_der = aux_eliminar->der;
		aux_izq = aux_eliminar->izq;
		aux_maxder = aux_eliminar->izq;
		while(aux_maxder->der != NULL){
			aux_antesmax = aux_maxder;
			aux_maxder = aux_maxder->der;
		}
		if(aux_maxder->izq == NULL){
			aux_eliminar = aux_maxder;
			aux_eliminar->izq = aux_izq;
			aux_eliminar->der = aux_der;
			aux_maxder = NULL;
		}
		else {
			aux_eliminar = aux_maxder;
			aux_eliminar->izq = aux_izq;
			aux_eliminar->der = aux_der;
			aux_antesmax->der = aux_maxder->izq;
			aux_maxder = NULL;			
			
		}
	}
}

void Arbol::print_preorden(Nodo *nodo){
	if(nodo == NULL){
		cout << "NULL";
		return;
	}else{
		cout << nodo->num << ", ";
		print_preorden(nodo->izq);
		print_preorden(nodo->der);
	}
}
void Arbol::print_inorden(Nodo *nodo){
	if(nodo == NULL){
		return;
	}else{
		print_inorden(nodo->izq);
		cout<< nodo->num << ", ";
		print_inorden(nodo->der);
	}
}
void Arbol::print_posorden(Nodo *nodo){
	if(nodo == NULL){
		return;
	}else{
		print_posorden(nodo->izq);
		print_posorden(nodo->der);
		cout<< nodo->num << ", ";
	}
}

Nodo* Arbol::get_raiz(){
	return this->raiz;
}


