//creamos el nodo como libreria .h
#ifndef NODO_H
#define NODO_H

typedef struct _Nodo {
    int num;
    struct _Nodo *izq;
    struct _Nodo *der;
} Nodo;
#endif
