#include "Arbol.h"

#ifndef MAKEARBOL_H
#define MAKEARBOL_H

class Makearbol {
    private:
        Arbol *arbol = NULL;

    public:
        /* constructor */
        Makearbol ();
        
        Arbol* get_arbol();
};
#endif
