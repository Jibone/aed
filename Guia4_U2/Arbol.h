#include <iostream>
#include "Nodo.h"
using namespace std;

#ifndef ARBOL_H
#define ARBOL_H

class Arbol {
    private:
        Nodo *raiz = NULL;

    public:
        /* constructor*/
        Arbol();
        
        /* crea un nuevo nodo, recibe un entero para la lista, y lo ordenará inmediatamente en la lista*/
		Nodo* crear_nodo (int num);
        void insertar_nodo (Nodo *&viejo, int num);
        Nodo* busqueda(Nodo *buscado, int num);
        void eliminar_nodo(Nodo *&eliminar, int num);
        void print_preorden(Nodo *nodo);
        void print_inorden(Nodo *nodo);
        void print_posorden(Nodo *nodo);
        Nodo* get_raiz();

};
#endif
