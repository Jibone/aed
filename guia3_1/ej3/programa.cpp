#include <iostream>
using namespace std;


/* clases */
#include "Lista.h"
#include "Crealista.h"
#include "Nodo.h"
void menu(){

	cout << "---------Menu---------" << endl;
	cout << "Mostrar postres    [1]" << endl;
	cout << "Agregar postre     [2]" << endl;
	cout << "Eliminar postre    [3]" << endl;
	cout << "Seleccionar postre [4]" << endl;
	cout << "----------------------" << endl;
}

/* función principal. */
int main (void) {
	int opcion;
	int num_postre;
	string postre_name;
	string ingrediente_name;
	bool ciclo = true;
	//Creamos las listas
    Crealista moldelista1 = Crealista();
    Lista *postres = moldelista1.get_lista();

    
    while(ciclo == true){
		cout << " " << endl;
		menu();
		cout << "Opcion: "; cin>>opcion;
		cout << " " << endl;
		switch(opcion){
			case 1:
				cout << "Así luce la carta hasta el momento: " << endl;
				postres->imprimir();
				break;
				
			case 2:
				cout << "Ingrese nombre del postre: " << endl;
				cin >> postre_name;
				postres->crear(postre_name);
				postres->ordena_lista();
				cout << " " << endl;
				break;
			
			case 3:	
				cout << "Ingrese el numero del postre que desea eliminar: " << endl;
				postres->imprimir();
				cin >> num_postre;
				postres->eliminar(num_postre);
				break;
				
				
		}
		
	}
    return 0;
}
