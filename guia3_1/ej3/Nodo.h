//creamos el nodo como libreria .h
//#define max_char 20
#ifndef NODO_H
#define NODO_H

typedef struct _Ingrediente {
	string nombre;
	struct _Ingrediente *sig;
} Ingrediente;

typedef struct _Postre {
	string nombre;
    struct _Postre *sig;
    struct _Ingrediente *ingred;
} Postre;
#endif
