#include <iostream>
#include "Nodo.h"
using namespace std;

#ifndef LISTA_H
#define LISTA_H

class Lista {
    private:
		Postre *raiz = NULL;
        Postre *ultimo = NULL;

    public:
        /* constructor*/
        Lista();
        
        /* crea un nuevo nodo, recibe un entero para la lista, y lo ordenará inmediatamente en la lista*/
        void crear (string nombre);
        void eliminar (int numero);
        /* imprime la lista. */
        void imprimir ();
        void ordena_lista();
        
};
#endif
