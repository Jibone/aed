#include <iostream>
#include <string.h>
using namespace std;

//#include "Nodo.h"
#include "Lista.h"

Lista::Lista() {}

//metodo que crea la lista de postres
void Lista::crear (string nombre) {
	
    Postre *tmp;
    //crea un nodo
    tmp = new Postre;
	//guarda en numero
    tmp->nombre = nombre;
    //crea el puntero al siguiente nodo como un NULL, indicando que es el ultimo
    tmp->sig = NULL;
	tmp->ingred = NULL;
    //si el es primer nodo de la lista, lo deja como raíz y como último nodo
    if (this->raiz == NULL) 
    { 
        this->raiz = tmp;
        this->ultimo = this->raiz;
    //de lo contrario, apunta el actual último nodo al nuevo y deja el nuevo como el último de la lista
    }
	else
	{
		this->ultimo->sig = tmp;
		this->ultimo = tmp;
	}
}

void Lista::eliminar (int numero) {
	//crea nodo temporal
    Postre *tmp = this->raiz;
    Postre *aux = this->raiz->sig;
    int cuenta = 1;

    //corre por la terminal los numeros
    while (tmp != NULL) {
		//elimina nodo si este es el unico en la lista
		if((numero == 1) && (this->raiz->sig == NULL)){
			this->raiz = NULL;
			this->ultimo = NULL;
			break;
		}
		//elimina nodo si es el primero de la lista
        if((numero == 1) && (this->raiz->sig != NULL)){
			this->raiz = aux;
			break;
        }
        //elimina nodo si esta al medio de la lista
        if((cuenta == numero - 1)&&(aux->sig != NULL)){
			tmp->sig = aux->sig;
			
			break;
		}
		//elimina nodo si es el ultimo
		if((cuenta == numero - 1)&&(aux->sig == NULL)){
			tmp->sig = NULL;
			this->ultimo = tmp;
			break;
		}	
        tmp = tmp->sig;
        aux = aux->sig;
        cuenta = cuenta + 1;
    }
}

//metodo que imprime la lista
void Lista::imprimir () {  
    //crea nodo temporal
    Postre *tmp = this->raiz;
    int cuenta = 1;

    //corre por la terminal los numeros
    while (tmp != NULL) {
        cout << cuenta << ". " << tmp->nombre << endl;
        tmp = tmp->sig;
        cuenta = cuenta + 1;
    }
    cout << " " << endl;
}

void Lista::ordena_lista ()
{
	Postre *tmp = this->raiz;
	Postre *aux = NULL;
	string aux_string;
	int comparador;
	
	while(tmp != NULL)
	{
		aux = tmp->sig;
		while(aux != NULL)
		{
			comparador = tmp->nombre.compare(aux->nombre);
			if (comparador > 0)
			{
				aux_string = aux->nombre;
				aux->nombre = tmp->nombre;
				tmp->nombre = aux_string;
			}
			aux = aux->sig;
		}
		tmp = tmp->sig;
	}
}

