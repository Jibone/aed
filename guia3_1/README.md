#LISTAS_ENLAZADAS
Para iniciar el programa se debe hacer desde la terminal, primero compilando dentro de la carpeta donde se encuentra el archivo makefile, utilizando el comando "makefile", para luego iniciar con "./programa".
1.-El programa genera una lista de numeros desordenados, los cuales el usuario le irá ingresando, para posteriormente hacer uso de la funcion dividir lista, en la cual se formarán 2 listas; una con los valores positivos y otra con los negativos (de la lista ingresada por el usuario).
2.-El programa recibe nombres como datos, los cuales va ingresando en una lista, la cual ordenará los nombres alfabeticamente.
3.-El programa permite recibir nombres de postres como datos, los cuales formarán una lista ordenada alfabeticamente, a esta lista le podemos tanto añadir como quitar postres. Dichos postres están enlazados a una lista que contiene los ingredientes de cada uno, a esta lista se le pueden tanto añadir como quitar ingredientes.
