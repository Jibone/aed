#include <iostream>
using namespace std;

//#include "Nodo.h"
#include "Lista.h"

Lista::Lista() {}

//metodo que crea la lista
void Lista::crear (int numero) {
    Nodo *tmp;

    //crea un nodo
    tmp = new Nodo;
	//guarda en numero
    tmp->numero = numero;
    //crea el puntero al siguiente nodo como un NULL, indicando que es el ultimo
    tmp->sig = NULL;

    //si el es primer nodo de la lista, lo deja como raíz y como último nodo
    if (this->raiz == NULL) 
    { 
        this->raiz = tmp;
        this->ultimo = this->raiz;
    //de lo contrario, apunta el actual último nodo al nuevo y deja el nuevo como el último de la lista
    }
	else
	{
		this->ultimo->sig = tmp;
		this->ultimo = tmp;
	}
}

//metodo que imprime la lista
void Lista::imprimir () {  
    //crea nodo temporal
    Nodo *tmp = this->raiz;

    //corre por la terminal los numeros
    while (tmp != NULL) {
        cout << "numero: " << tmp->numero << endl;
        tmp = tmp->sig;
    }
}

//metodo que ordena la lista
void Lista::ordena_lista ()
{
	Nodo *tmp = this->raiz;
	Nodo *aux = NULL;
	int numero_aux;
	while(tmp != NULL)
	{
		aux = tmp->sig;
		while(aux != NULL)
		{
			if (tmp->numero > aux->numero)
			{
				numero_aux = aux->numero;
				aux->numero = tmp->numero;
				tmp->numero = numero_aux;
			}
			aux = aux->sig;
		}
		tmp = tmp->sig;
	}
}

//metodo que crea una lista con los positivos de la lista ingresada
void Lista::separa_pos (Lista lista[])
{
	 //crea nodo temporal
    Nodo *tmp = lista->raiz;

    //recorre los nodos de la lista ingresada, y los añade a una nueva lista si es que son mayores o igual a 0
    while (tmp != NULL) {
        if(tmp->numero >= 0){
			crear(tmp->numero);
		}
        tmp = tmp->sig;
    }
}

//metodo que crea una lista con los negativos de la lista ingresada
void Lista::separa_neg (Lista lista[])
{
	 //crea nodo temporal
    Nodo *tmp = lista->raiz;

    //recorre los nodos de la lista ingresada, y los añade a una nueva lista si son menores que 0
    while (tmp != NULL) {
        if(tmp->numero < 0){
			crear(tmp->numero);
		}
        tmp = tmp->sig;
    }
}

