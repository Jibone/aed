#include <iostream>
#include "Nodo.h"
using namespace std;

#ifndef LISTA_H
#define LISTA_H

class Lista {
    private:
        Nodo *raiz = NULL;
        Nodo *ultimo = NULL;

    public:
        /* constructor*/
        Lista();
        
        /* crea un nuevo nodo, recibe un entero para la lista, y lo ordenará inmediatamente en la lista*/
        void crear (int numero);
        /* imprime la lista. */
        void imprimir ();
        void ordena_lista();
        
        //estos metodos sirven para separar la lista que se les ingrese
        void separa_pos(Lista lista[]);
        void separa_neg(Lista lista[]);
};
#endif
