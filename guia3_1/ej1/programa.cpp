#include <iostream>
using namespace std;


/* clases */
#include "Lista.h"
#include "Crealista.h"
#include "Nodo.h"
void menu(){

	cout << "--------Menu---------" << endl;
	cout << "Ingresar dato     [1]" << endl;
	cout << "Dividir lista     [2]" << endl;
	cout << "---------------------" << endl;
}

/* función principal. */
int main (void) {
	bool ciclo = true;
	int ingreso;
	int opcion;
	//Creamos las listas
    Crealista moldelista1 = Crealista();
    Crealista moldelista_pos = Crealista();
    Crealista moldelista_neg = Crealista();
    Lista *lista1 = moldelista1.get_lista();
    Lista *lista_pos = moldelista_pos.get_lista();
    Lista *lista_neg = moldelista_neg.get_lista();
    
    while(ciclo == true){
		cout << " " << endl;
		menu();
		cout << "Opcion: "; cin>>opcion;
		cout << " " << endl;
		switch(opcion){
			case 1:
				cout << "Ingrese un numero en la lista 1:" << endl;
				cin >> ingreso;
				lista1->crear(ingreso);
				cout << " " << endl;
				cout << "Así luce la lista hasta el momento" << endl;
				lista1->imprimir();
				break;
			case 2:
				lista_pos->separa_pos(lista1);
				cout << "Así queda la lista solo con positivos: " << endl;
				lista_pos->ordena_lista();
				lista_pos->imprimir();
				cout << " " << endl;
				
				lista_neg->separa_neg(lista1);
				cout << "Así queda la lista solo con negativos: " << endl;
				lista_neg->ordena_lista();
				lista_neg->imprimir();
				
				break;
				
		}
		
	}
    return 0;
}
