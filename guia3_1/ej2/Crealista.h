#include "Lista.h"

#ifndef CREALISTA_H
#define CREALISTA_H

class Crealista {
    private:
        Lista *lista = NULL;

    public:
        /* constructor */
        Crealista ();
        
        Lista* get_lista();
};
#endif
