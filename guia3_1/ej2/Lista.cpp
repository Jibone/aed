#include <iostream>
#include <string.h>
using namespace std;

//#include "Nodo.h"
#include "Lista.h"

Lista::Lista() {}

//metodo que crea la lista
void Lista::crear (string nombre) {
    Nodo *tmp;

    //crea un nodo
    tmp = new Nodo;
	//guarda en numero
    tmp->nombre = nombre;
    //crea el puntero al siguiente nodo como un NULL, indicando que es el ultimo
    tmp->sig = NULL;

    //si el es primer nodo de la lista, lo deja como raíz y como último nodo
    if (this->raiz == NULL) 
    { 
        this->raiz = tmp;
        this->ultimo = this->raiz;
    //de lo contrario, apunta el actual último nodo al nuevo y deja el nuevo como el último de la lista
    }
	else
	{
		this->ultimo->sig = tmp;
		this->ultimo = tmp;
	}
}

//metodo que imprime la lista
void Lista::imprimir () {  
    //crea nodo temporal
    Nodo *tmp = this->raiz;
    int cuenta = 1;

    //corre por la terminal los numeros
    while (tmp != NULL) {
        cout << cuenta << ". " << tmp->nombre << endl;
        tmp = tmp->sig;
        cuenta = cuenta + 1;
    }
}

void Lista::ordena_lista ()
{
	Nodo *tmp = this->raiz;
	Nodo *aux = NULL;
	string aux_string;
	int comparador;
	
	while(tmp != NULL)
	{
		aux = tmp->sig;
		while(aux != NULL)
		{
			comparador = tmp->nombre.compare(aux->nombre);
			if (comparador > 0)
			{
				aux_string = aux->nombre;
				aux->nombre = tmp->nombre;
				tmp->nombre = aux_string;
			}
			aux = aux->sig;
		}
		tmp = tmp->sig;
	}
}

