#include <iostream>
using namespace std;

#ifndef LISTA_H
#define LISTA_H

class Lista {
    private:
        Nodo *raiz = NULL;
        Nodo *ultimo = NULL;

    public:
        /* constructor*/
        Lista();
        
        /* crea un nuevo nodo, recibe un entero para la lista*/
        void crear (int num);
        /* imprime la lista. */
        void imprimir ();
        void unir (Nodo* tmp);
        Nodo* get_raiz();
};
#endif
