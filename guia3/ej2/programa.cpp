#include <iostream>
using namespace std;

/* definición de la estructura Nodo. */
#include "Nodo.h"

/* clases */
#include "Lista.h"
void menu(){

	cout << "--------Menu---------" << endl;
	cout << "Ingresar dato lista1 [1]" << endl;
	cout << "Ingresar dato lista2 [2]" << endl;
	cout << "Mezclar listas	      [3]" << endl;
	cout << "---------------------" << endl;
}
/* intenté crear esta funcion para rellenar la lista3 con los datos de las anteriores, pero no pude recorrer
 * las listas 1 y 2 desde esta clase (programa.cpp) e ir ingresando sus valores en la lista 3. Así que hice una
 * pequeña "trampita", que a medida que se ingresan valores en la lista 1 y 2, tambien lo hacen en la 3.
 * Cabe decir que se ordenan apenas entra
void mezclar(Lista lista1, Lista lista2, Lista lista3){
	Nodo *ingresador;
	ingresador = new Nodo;
	ingresador = lista1.get_raiz();
	//lista3 = lista1;
	while(ingresador->sig != NULL){
		lista3.crear(ingresador->num);
		ingresador = ingresador->sig;
	}
	//while(ingresador->sig != NULL){
//		lista3.unir(ingresador);
	//	ingresador = ingresador->sig;
//	}	
}		
*/
/* función principal. */
int main (void) {
	int opcion;
	int ingreso;
		//Creamos las lista
    Lista lista1 = Lista();
    Lista lista2 = Lista();
    Lista lista3 = Lista();
    
    bool ciclo = true;
	while(ciclo == true){
		cout << " " << endl;
		menu();
		cout << "Opcion: "; cin>>opcion;
		cout << " " << endl;
		switch(opcion){
			case 1:
				cout << "Ingrese un numero en la lista 1:" << endl;
				cin >> ingreso;
				lista1.crear(ingreso);
				lista3.crear(ingreso);
				lista1.imprimir();
				break;
			case 2:
				cout << "Ingrese un numero en la lista 2:" << endl;
				cin >> ingreso;
				lista2.crear(ingreso);
				lista3.crear(ingreso);
				lista2.imprimir();
				break;
			case 3:
				//mezclar(lista1, lista2, lista3);
				lista3.imprimir();	
				
				break;
		}
		
	}
    return 0;
}
