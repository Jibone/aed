#include <iostream>
using namespace std;

#include "Nodo.h"
#include "Lista.h"

Lista::Lista() {}

void Lista::crear (int num) {
    Nodo *tmp;
	Nodo *aux;
	Nodo *aux2;
    /* creamos 3 nodos, tmp será el nuevo valor ingresado, y aux y aux2 nos serviran para recorrer
       la lista mientras vamos comparando valores y así ordenar apenas ingresa el numero*/
    tmp = new Nodo;
    aux = new Nodo;
    aux2 = new Nodo;
    /* asigna el valor del entero en la estructura Nodo */
    tmp->num = num;
    /* apunta a NULL por defecto. */
    tmp->sig = NULL;
    /* si el es primer nodo de la lista, lo deja como raíz y como último nodo. */
    if (this->raiz == NULL) { 
        this->raiz = tmp;
        this->ultimo = this->raiz;
    } 
     /* de lo contrario verificará si va al inicio de la lista, al final, o entre medio*/
    else {
		//aqui ve si va al principio
		if(tmp->num <= this->raiz->num){
			tmp->sig = this->raiz;
			this->raiz = tmp;	
		}
		else {
			//aqui ve si va al final
			if(tmp->num >= this->ultimo->num){
				this->ultimo->sig = tmp;
				this->ultimo = tmp;
			}
			//y si ninguna de las condiciones anteriores se cumple, significa que va al medio
			else {
				aux = this->raiz;
				aux2 = aux->sig;
				while(aux2 != NULL){
					if(tmp->num > aux->num && tmp->num <= aux2->num){
						aux->sig = tmp;
						tmp->sig = aux2;
						break;
					}
					aux = aux->sig;
					aux2 = aux2->sig;	
				}
			}
		}		
	}
}

/* otra funcion que no pude implementar
void Lista::unir (Nodo* tmp) {
	
	Nodo *aux;
	Nodo *aux2;
    // creamos 2 nodos, tmp será el nuevo valor ingresado, y aux y aux2 nos serviran para recorrer
      // la lista mientras vamos comparando valores y así ordenar apenas ingresa el numero
    aux = new Nodo;
    aux2 = new Nodo;
    tmp->sig = NULL;
    // si el es primer nodo de la lista, lo deja como raíz y como último nodo. 
    if (this->raiz == NULL) { 
        this->raiz = tmp;
        this->ultimo = this->raiz;
    } 
     // de lo contrario verificará si va al inicio de la lista, al final, o entre medio
    else {
		//aqui ve si va al principio
		if(tmp->num <= this->raiz->num){
			tmp->sig = this->raiz;
			this->raiz = tmp;	
		}
		else {
			//aqui ve si va al final
			if(tmp->num >= this->ultimo->num){
				this->ultimo->sig = tmp;
				this->ultimo = tmp;
			}
			//y si ninguna de las condiciones anteriores se cumple, significa que va al medio
			else {
				aux = this->raiz;
				aux2 = aux->sig;
				while(aux2 != NULL){
					if(tmp->num > aux->num && tmp->num <= aux2->num){
						aux->sig = tmp;
						tmp->sig = aux2;
						break;
					}
					aux = aux->sig;
					aux2 = aux2->sig;	
				}
			}
		}		
	}
}
*/
void Lista::imprimir () {  
    /* utiliza variable temporal para recorrer la lista. */
    Nodo *tmp = this->raiz;

    /* la recorre mientras sea distinto de NULL (no hay más nodos). */
    cout << " " << endl;
    cout << "Así luce su lista actualmente: " << endl;
    while (tmp != NULL) {
        cout << tmp->num << endl;
        tmp = tmp->sig;
    }
}

Nodo* Lista::get_raiz () {
	return this->raiz;
}	
