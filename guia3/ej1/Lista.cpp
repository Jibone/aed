#include <iostream>
using namespace std;

#include "Nodo.h"
#include "Lista.h"

Lista::Lista() {}

void Lista::crear (int num) {
    Nodo *tmp;
	Nodo *aux;
	Nodo *aux2;
    /* creamos 3 nodos, tmp será el nuevo valor ingresado, y aux y aux2 nos serviran para recorrer
       la lista mientras vamos comparando valores y así ordenar apenas ingresa el numero*/
    tmp = new Nodo;
    aux = new Nodo;
    aux2 = new Nodo;
    /* asigna el valor del entero en la estructura Nodo */
    tmp->num = num;
    /* apunta a NULL por defecto. */
    tmp->sig = NULL;
    /* si el es primer nodo de la lista, lo deja como raíz y como último nodo. */
    if (this->raiz == NULL) { 
        this->raiz = tmp;
        this->ultimo = this->raiz;
    } 
     /* de lo contrario verificará si va al inicio de la lista, al final, o entre medio*/
    else {
		//aqui ve si va al principio
		if(tmp->num <= this->raiz->num){
			tmp->sig = this->raiz;
			this->raiz = tmp;	
		}
		else {
			//aqui ve si va al final
			if(tmp->num >= this->ultimo->num){
				this->ultimo->sig = tmp;
				this->ultimo = tmp;
			}
			//y si ninguna de las condiciones anteriores se cumple, significa que va al medio
			else {
				aux = this->raiz;
				aux2 = aux->sig;
				while(aux2 != NULL){
					if(tmp->num > aux->num && tmp->num <= aux2->num){
						aux->sig = tmp;
						tmp->sig = aux2;
						break;
					}
					aux = aux->sig;
					aux2 = aux2->sig;	
				}
			}
		}		
	}
}
//no se necesitó de esta funcion (que ahora mismo no funciona, ya que intenté ordenar usando los
//valores de los nodos mas que como estructura, despues intenté con un nodo que no fuese puntero, 
//pero finalmente los ordené a medida que ingresaban, sin hacer uso de la funcion ordenar
/*void Lista::ordenar () {
	Nodo *tmp = this->raiz;
	Nodo *aux = this->raiz->sig;
	Nodo auxinfo = new Nodo;
	while(tmp->sig != NULL) {
          aux = tmp->sig;
          
          while(aux!=NULL)
          {
               if(tmp->num > aux->num)
               {
                    auxinfo = tmp->sig;
                    aux->num = tmp->num;
                    tmp->num = auxinfo->num;          
               }
               aux = aux->sig;                    
          }    
          tmp = tmp->sig;
          aux = tmp->sig;
           
     }
     
}*/
	
void Lista::imprimir () {  
    /* utiliza variable temporal para recorrer la lista. */
    Nodo *tmp = this->raiz;

    /* la recorre mientras sea distinto de NULL (no hay más nodos). */
    while (tmp != NULL) {
        cout << "Numero: " << tmp->num << endl;
        tmp = tmp->sig;
    }
}
