#include <iostream>
using namespace std;

/* definición de la estructura Nodo. */
#include "Nodo.h"

/* clases */
#include "Lista.h"
/* función principal. */
int main (void) {
	bool ciclo = true;
	//Creamos la lista
    Lista lista = Lista();
    
    while(ciclo == true){
		
		int ingreso;
		cout << "Ingrese un numero entero en la lista: " << endl;
		cin >> ingreso;
		lista.crear(ingreso);
		cout << "Así luce ahora mismo la lista: " << endl;
		lista.imprimir();
	}
    return 0;
}
