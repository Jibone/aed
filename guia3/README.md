#LISTAS_ENLAZADAS
Para iniciar el programa se debe hacer desde la terminal, primero compilando dentro de la carpeta donde se encuentra el archivo makefile, utilizando el comando "makefile", para luego iniciar con "./programa".
1.- El programa genera una lista enlazada con nodos con valores enteros a medida que el usuario va ingresando los valores. No presenta alguna funcion de "ordenar" ya que lo va haciendo a medida que se le ingresan los valores.
2.-Se busca generar 2 listas con valores ordenados, y luego "mezclarlas" en una 3ra lista, sin embargo no pasar los nodos o valores a de una lista a otra, es por esto que a medida que se ingresan datos en las 2 primeras listas, tambien van siendo ingresados en la 3ra lista (mezcla).
