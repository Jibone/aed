-El siguiente programa tiene como funcion calcular el tiempo que toma en ejecutarse distintos metodos de ordenamiento de listas, para de esta manera ver que tan eficiente son.
-Para utilizarlo se debe ubicar en la carpeta contenedora a través de la terminal, una vez allí se debe compilar mediante el uso del comando "make". En este punto ya se puede iniciar el programa con << ./programa N opcion >> donde N es la cantidad de datos que se desean generar y ordenar (desde 1 hasta 1000000), y opcion es una letra que puede ser s o n (s si desea ver la lista ordenada, y n si no desea verla).
-El programa se encuentra desarrollado en C++.

Hecho por José Bernal.






