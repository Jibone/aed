#include <iostream>
#include <time.h>
#include <stdio.h>
#include <stdlib.h>
#include "Ordenador.h"

using namespace std;

void menu(int valores[], int N, bool VER){
	Ordenador ordenar;
	double tiempos[7] = {ordenar.burbuja_menor(valores, N, VER), ordenar.burbuja_mayor(valores, N, VER), ordenar.insercion(valores, N, VER), ordenar.insercion_binaria(valores, N, VER), ordenar.seleccion(valores, N, VER)};
	cout << "--------------------------------------------------" << endl;
	cout << "Metodo                  |Tiempo                   " << endl;
	cout << "--------------------------------------------------" << endl;
	cout << "Burbuja Menor           |" << tiempos[0] << endl;
	cout << "Burbuja Mayor           |" << tiempos[1] << endl;
	cout << "Inserción               |" << tiempos[2] << endl;
	cout << "Inserción binaria       |" << tiempos[3] << endl;
	cout << "Selección               |" << tiempos[4] << endl;
	cout << "Shell                   |                         " << endl;
	cout << "Quicksort               |                         " << endl;

}

int main(int argc, char *argv[]){
	Ordenador ordenar;
	//Se leen los valores de entrada y utilizamos a temp para darle un valor a VER
	int N = stoi(argv[1]);
	string temp = argv[2];
	bool VER;
	
	if(temp.compare("s") == 0){
		VER = true;
		//cout << "TRUE" << endl;
	}
	else if (temp.compare("n") == 0){
		VER = false;
		//cout << "FALSE" << endl;
	}
	else{
		 cout << "Ingrese parametros validos" << endl;
		 exit(0);
	}
	
	if(N<0 || N>1000000){
		cout << "-----El numero ingresado no es permitido-----" << endl;
		cout << "Reinicie el programa e intente con otro valor." << endl;
		exit(0);
	}
 
	//El tamaño vector se ve limitado por el if que está arriba, de modo que si N es mayor a 1.000.000 el programa se cerraría.
	int valores[N];
	
	//aqui se generan los valores random usando como semilla la hora del pc
	srand(time(NULL));
	for(int i=0; i<N; i++){
		valores[i] = rand() % 1000000;
	//	cout << << valores[i] << endl;
	}
	if(VER == true){
		cout << "La lista sin ordenar sería: " << endl;
		for(int i=0; i<N; i++){
		cout << "Original["<< i <<"]=" << valores[i] << endl;
		
		}
		cout << "------------------------" << endl;
		cout << "Listas ordenadas por cada método: " << endl;
	}
	
	menu(valores, N, VER);
	//ordenar.burbuja_menor(valores, N, VER);
	//ordenar.burbuja_mayor(valores, N, VER);
	//ordenar.insercion(valores, N, VER);
	//ordenar.insercion_binaria(valores, N, VER);
	//ordenar.seleccion(valores, N, VER);
	










}
