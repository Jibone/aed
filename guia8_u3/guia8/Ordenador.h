#include <iostream>
using namespace std;

#ifndef ORDENADOR_H
#define ORDENADOR_H

class Ordenador {
	public:
		Ordenador();
		double burbuja_menor(int [], int, bool);
		double burbuja_mayor(int [], int, bool); 
		double insercion(int [], int, bool); 
		double insercion_binaria(int [], int, bool);
		double seleccion(int [], int, bool);/*
		int shell(int [], int);
		int quicksort(int [], int); */
		void imprimir(int [], int);
		
		
		
};
#endif
