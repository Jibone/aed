#include <iostream>
#include <time.h>
#include "Ordenador.h"

Ordenador::Ordenador() {}

double Ordenador::burbuja_menor(int original[], int N, bool VER){
	int valores[N];
	for(int i=0; i<N; i++){
		valores[i] = original[i];
	}
	int aux;
	double t_inicio, t_final, tiempo;
	
    t_inicio = clock();
    for(int i=0; i<N ; i++){
		for(int j=i+1; j<N ; j++){
			if(valores[i]>valores[j]){
				aux = valores[i];
				valores[i] = valores[j];
				valores[j] = aux;
			}
		}
	}
	t_final = clock();
	
	tiempo = ((t_final-t_inicio)/ (double) CLOCKS_PER_SEC)*1000;
		
	if(VER == true){
		cout << "Burbuja Menor: " << tiempo << " ms" <<endl;
		imprimir(valores, N);
		cout << "------------------------" << endl;
	}
	
	return tiempo;
		

}
double Ordenador::burbuja_mayor(int original[], int N, bool VER){
	int valores[N];
	for(int i=0; i<N; i++){
		valores[i] = original[i];
	}
	int aux;
	double t_inicio, t_final, tiempo;
	
    t_inicio = clock();
    for(int i=N; i>=0; i--){
		for(int j=N-1; j>=0 ; j--){
			if(valores[i]>valores[j]){
				aux = valores[i];
				valores[i] = valores[j];
				valores[j] = aux;
			}
		}
	}
	t_final = clock();
	
	tiempo = ((t_final-t_inicio)/ (double) CLOCKS_PER_SEC)*1000;
		
	if(VER == true){
		cout << "Burbuja Mayor: " << tiempo << " ms" <<endl;
		imprimir(valores, N);
		cout << "------------------------" << endl;
	}
	
	return tiempo;
}

double Ordenador::insercion(int original[], int N, bool VER){
	int valores[N];
	for(int i=0; i<N; i++){
		valores[i] = original[i];
	}
	int aux;
	double t_inicio, t_final, tiempo;
	
	t_inicio = clock();
	for (int i=0; i<N; i++){
		aux = valores[i];
		int j = i - 1;
        while ( (valores[j] > aux) && (j >= 0) ){
			valores[j+1] = valores[j];
			j--;
			valores[j+1] = aux;
		}
	}
	t_final = clock();
	
	tiempo = ((t_final-t_inicio)/ (double) CLOCKS_PER_SEC)*1000;
		
	if(VER == true){
		cout << "Inserción: " << tiempo << " ms" <<endl;
		imprimir(valores, N);
		cout << "------------------------" << endl;
	}
	return tiempo;
}

double Ordenador::insercion_binaria(int original[], int N, bool VER){
	int valores[N];
	int i,j,aux,izq,der,m;
	
	for(int i=0; i<N; i++){
		valores[i] = original[i];
	}
	double t_inicio, t_final, tiempo;
	
	t_inicio = clock();
    for(i=1;i<N;i++){
		aux = valores[i];
		izq=0;
		der=i-1;
		while(izq<=der){
            m=((izq+der)/2);
            if (aux<valores[m]){
                der=m-1;
            }
            else{
				izq=m+1;              
            }
			j=i-1;
		}
		while(j>=izq){
            valores[j+1]=valores[j];
			j=j-1;
		}
		valores[izq]=aux;
	}
	t_final = clock();
	
	tiempo = ((t_final-t_inicio)/ (double) CLOCKS_PER_SEC)*1000;
		
	if(VER == true){
		cout << "Inserción Binaria: " << tiempo << " ms" <<endl;
		imprimir(valores, N);
		cout << "------------------------" << endl;
	}
	return tiempo;
    
}

double Ordenador::seleccion(int original[], int N, bool VER){

	int min,i,j,aux;
	int valores[N];
	
	for(int i=0; i<N; i++){
		valores[i] = original[i];
	}
	double t_inicio, t_final, tiempo;

	t_inicio = clock();
	for (i=0; i<N-1; i++) 
	{
		  min=i;
		  
		  for(j=i+1; j<N; j++)
				if(valores[min] > valores[j])
				   min=j;
		  aux=valores[min];
		  valores[min]=valores[i];
		  valores[i]=aux ;
	}
	t_final = clock();
		
	tiempo = ((t_final-t_inicio)/ (double) CLOCKS_PER_SEC)*1000;
	
	if(VER == true){
		cout << "Selección: " << tiempo << " ms" <<endl;
		imprimir(valores, N);
		cout << "------------------------" << endl;
	}
	return tiempo;

}



void Ordenador::imprimir(int valores[], int N){
	for(int i=0; i<N; i++){
			cout << "Lista["<< i <<"]=" << valores[i] << endl;
		}
}

