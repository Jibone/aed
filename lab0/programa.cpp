#include <iostream>
#include <list>
#include <string>
#include "Aminoacido.h"
#include "Atomo.h"
#include "Coordenada.h"
#include "Proteina.h"
#include "Proteina.cpp"
#include "Cadena.h"
using namespace std;
void print_proteina(list<Proteina> proteina) {


	cout << "~~~~~~~~~~~~~Lector Proteico~~~~~~~~~~~~~" << endl;
	cout << " " << endl;
	for(Proteina prot: proteina){
		cout << "Nombre de la proteina: " << prot.get_nombre() << endl;
		cout << "ID de la proteina: " << prot.get_id() << endl;

		for(Cadena cad: prot.get_cadenas() ){
			cout << "Letra de la Cadena: " << cad.get_letra() << endl;

			for(Aminoacido am: cad.get_aminoacidos() ){
				cout << "Nombre Aminoacido: " << am.get_nombre() << endl;
				cout << "Numero del Aminoacido: " << am.get_numero() << endl;
				
				for(Atomo at: am.get_atomos() ){
					cout <<"Nombre del Atomo: " << at.get_nombre() << endl;
					cout <<"Numero del Atomo: " << at.get_numero() << endl;
					Coordenada coord = at.get_coordenada();
					cout << "Coordenada X: " << coord.get_x() << endl;
					cout << "Coordenada Y: " << coord.get_y() << endl;
					cout << "Coordenada Z: " << coord.get_z() << endl;
				}
			}
		}
		cout << "~~~~~~~~~~~~~~~~~~~~~~~" << endl;
	}
}

int main (){
	list<Proteina> protolist;
	string cant_prot;
	string nombreprot;
	string idprot;
	string cant_cadenas;
	string cant_amino;
	string amino_nombre;
	string cant_atomos;
	string nombre_atomo;
	string x;
	string y;
	string z;
	
	
	cout <<"¿Cuantas proteinas ingresara?: ";
	getline(cin, cant_prot);
	for(int l=0; l<stoi(cant_prot); l++){
		cout << "Ingrese el nombre de su proteina" << endl;
		getline(cin, nombreprot);
		cout << "Ingrese el id de su proteina" << endl;
		getline(cin, idprot);
		Proteina proteina = Proteina(idprot, nombreprot);
		cout << "Cuantas cadenas tiene su proteina?" << endl;
		getline(cin, cant_cadenas);
		for(int i=0; i<(stoi(cant_cadenas)); i++){
			string letracad;
			cout << "Cual es la letra de su cadena numero " << i+1 << "?" << endl;
			getline(cin, letracad);
			Cadena cadena = Cadena(letracad);
			//proteina.add_cadena(cadena);
			
			cout << "Cuantos aminoacidos tiene su cadena " << letracad << "?" << endl; 
			getline(cin, cant_amino);
			for(int j=0; j<(stoi(cant_amino)); j++){
				cout << "Que aminoacido es el numero " << j+1 << "?" << endl;
				getline(cin, amino_nombre);
				Aminoacido aminoacido(amino_nombre, j+1);
				cout << "Cuantos atomos componen dicho aminoacido?" << endl;
				getline(cin, cant_atomos);
				for(int k=0; k<(stoi(cant_atomos)); k++){
					cout << "Que atomo es el numero " << k+1 << " de su aminoacido numero " << j+1 << "?" << endl;
					getline(cin, nombre_atomo);
					Atomo atomo = Atomo(nombre_atomo, k+1);
					cout <<"Cual es la coordenada x del Atomo?";
					getline(cin, x);
					cout <<"Cual es la coordenada y del Atomo?";
					getline(cin, y);
					cout <<"Cual es la coordenada z del Atomo?";
					getline(cin, z);
					Coordenada coordenada = Coordenada(stof(x), stof(y), stof(z));
					atomo.set_coordenada(coordenada);
					aminoacido.add_atomo(atomo);
				}
			cadena.add_aminoacido(aminoacido);		
			}
		proteina.add_cadena(cadena);
				
		}
		protolist.push_back(proteina);		
	}
print_proteina(protolist);			
}			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
		
	
	
