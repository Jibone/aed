#include <iostream>
using namespace std;
#include "Profesor.h"
#include <math.h>

float edad_promedio(Profesor profes[],int cant){
	float promedio= 0;
	for (int i=0; i<cant; i++){
		promedio= promedio + profes[i].get_edad();
	}
	cant = (float) cant;
	promedio= promedio/cant;
	cout << "La edad promedio es: " << promedio << endl;	
}


int main(){
	int cant;
	string nombre;
	string sexo;
	int edad;
	int promedio;
	cout << "¿Cuantos profesores trabajan en la universidad?" << endl; cin>>cant;
	Profesor profes[cant];
	for(int i=0; i<cant; i++){
		cout << "Nombre del profesor " << i+1 << ":" << endl; cin>>nombre;
		cout << "Sexo(hombre, mujer) del profesor " << i+1 << ":" << endl; cin>>sexo;
		cout << "Edad del profesor " << i+1 << ":" << endl; cin>>edad;
		profes[i]= Profesor(nombre, sexo, edad);
	}
	edad_promedio(profes, cant);
}


