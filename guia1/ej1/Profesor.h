#include <iostream>
using namespace std;

#ifndef PROFESOR_H
#define PROFESOR_H

class Profesor {
    private:
        string nombre;
        string sexo;
        int edad;

    public:
        /* constructores */
        Profesor ();
        Profesor (string nombre, string sexo, int edad);
        
        /* métodos get and set */
        string get_nombre();
        
        string get_sexo();
        
        int get_edad();

        void set_nombre(string nombre);
        
        void set_sexo(string sexo);
        
        void set_edad(int edad);
        
};
#endif
