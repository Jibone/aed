#include <iostream>
using namespace std;
#include "Profesor.h"

Profesor::Profesor(){}
Profesor::Profesor(string nombre, string sexo, int edad){
	this->nombre=nombre;
	this->sexo=sexo;
	this->edad=edad;
}
string Profesor::get_nombre() {
	return this->nombre;
}
        
string Profesor::get_sexo() {
	return this->sexo;
}

int Profesor::get_edad() {
	return this->edad;
}

void Profesor::set_nombre(string nombre) {
	this->nombre = nombre;
}

void Profesor::set_sexo(string sexo) {
	this->sexo = sexo;
}

void Profesor::set_edad(int edad) {
	this->edad = edad;
}
