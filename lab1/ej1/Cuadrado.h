#include <iostream>
using namespace std;


#ifndef CUADRADO_H
#define CUADRADO_H

class Cuadrado {
    private:
		int tamano;
        int temp;
        int resultado=0;
        

    public:
        /* constructores */
        Cuadrado(int tamano);
        /* métodos get and set */
        void llenar();
        void suma(int numbers[]);
        int get_resultado();
        
};
#endif
