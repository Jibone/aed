#include <iostream>
using namespace std;
#include "Cuadrado.h"

int main(){
	int tamano;
	cout << "Ingrese el tamaño que tendrá su lista" <<endl; cin >>tamano;
	Cuadrado cuadrado = Cuadrado(tamano);
	cuadrado.llenar();
	cout << "La suma de los cuadrados de su lista es " << cuadrado.get_resultado() << endl;
}
